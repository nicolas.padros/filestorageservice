# ImageFileStorageService
This service is the implementation of the task requested by Agile Engine by Nicolás Padrós

## Possible improvements
* Should've used Redis for KeyValue store in memory, so that if the server fails, the data is not deleted.
Due to limitations in time this was implemented as a simple scala.List
* Better Error Handling with `.recoverWith()` (time limitations)
* Getting paginated images from the server could be done in parallel to increase performance. 
Because we did not know when the server would return the empty list of images, it was done sequentually to know when to stop getting images 
* Singletons for ImageCache and ImageFetcher should be considered
* Unit tests and integration tests
* `.idea/` folder should have been ignored


## Running

```bash
sbt run
```

And then go to <http://localhost:9000/search/${searchQuery}> to search from the image cache.


