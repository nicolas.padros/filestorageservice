// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/nicolaspadros/Documents/projects/metro/data-platform/FileSystemService/conf/routes
// @DATE:Sat Dec 19 11:55:58 ART 2020


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
