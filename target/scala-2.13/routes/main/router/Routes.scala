// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/nicolaspadros/Documents/projects/metro/data-platform/FileSystemService/conf/routes
// @DATE:Sat Dec 19 11:55:58 ART 2020

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:8
  PicturesController_0: controllers.PicturesController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:8
    PicturesController_0: controllers.PicturesController
  ) = this(errorHandler, PicturesController_0, "/")

  def withPrefix(addPrefix: String): Routes = {
    val prefix = play.api.routing.Router.concatPrefix(addPrefix, this.prefix)
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, PicturesController_0, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.PicturesController.init"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """search/""" + "$" + """searchTerm<[^/]+>""", """controllers.PicturesController.findPictures(searchTerm:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:8
  private[this] lazy val controllers_PicturesController_init0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_PicturesController_init0_invoker = createInvoker(
    PicturesController_0.init,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PicturesController",
      "init",
      Nil,
      "GET",
      this.prefix + """""",
      """""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_PicturesController_findPictures1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("search/"), DynamicPart("searchTerm", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PicturesController_findPictures1_invoker = createInvoker(
    PicturesController_0.findPictures(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PicturesController",
      "findPictures",
      Seq(classOf[String]),
      "GET",
      this.prefix + """search/""" + "$" + """searchTerm<[^/]+>""",
      """""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:8
    case controllers_PicturesController_init0_route(params@_) =>
      call { 
        controllers_PicturesController_init0_invoker.call(PicturesController_0.init)
      }
  
    // @LINE:9
    case controllers_PicturesController_findPictures1_route(params@_) =>
      call(params.fromPath[String]("searchTerm", None)) { (searchTerm) =>
        controllers_PicturesController_findPictures1_invoker.call(PicturesController_0.findPictures(searchTerm))
      }
  }
}
