// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/nicolaspadros/Documents/projects/metro/data-platform/FileSystemService/conf/routes
// @DATE:Sat Dec 19 11:55:58 ART 2020

import play.api.mvc.Call


import _root_.controllers.Assets.Asset

// @LINE:8
package controllers {

  // @LINE:8
  class ReversePicturesController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:9
    def findPictures(searchTerm:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "search/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("searchTerm", searchTerm)))
    }
  
    // @LINE:8
    def init(): Call = {
      
      Call("GET", _prefix)
    }
  
  }


}
