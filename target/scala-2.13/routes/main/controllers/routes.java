// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/nicolaspadros/Documents/projects/metro/data-platform/FileSystemService/conf/routes
// @DATE:Sat Dec 19 11:55:58 ART 2020

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReversePicturesController PicturesController = new controllers.ReversePicturesController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReversePicturesController PicturesController = new controllers.javascript.ReversePicturesController(RoutesPrefix.byNamePrefix());
  }

}
