// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/nicolaspadros/Documents/projects/metro/data-platform/FileSystemService/conf/routes
// @DATE:Sat Dec 19 11:55:58 ART 2020

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset

// @LINE:8
package controllers.javascript {

  // @LINE:8
  class ReversePicturesController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:9
    def findPictures: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PicturesController.findPictures",
      """
        function(searchTerm0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "search/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("searchTerm", searchTerm0))})
        }
      """
    )
  
    // @LINE:8
    def init: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PicturesController.init",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }


}
