package controllers

import business.{PictureCacheImpl, PictureFetcher}
import javax.inject.Inject
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc.{AbstractController, ControllerComponents}

import scala.concurrent.{ExecutionContext, Future}

/**
 * Implements the only method in the API: findPictures
 * @param cc components
 * @param ws WSClient to make request to server
 * @param exec Context for Futures execution
 */
class PicturesController @Inject()(cc: ControllerComponents, ws: WSClient)(implicit exec: ExecutionContext)
  extends AbstractController(cc) {


  val pictureFetcher = new PictureFetcher(ws)

  val pictureCache = new PictureCacheImpl(pictureFetcher)




  def init = Action.async {
    Future.successful(Ok)
  }


  /**
   * Finds pictures matching the searchTerm provided by the user
   * @param searchTerm string to match
   * @return the list of images that match
   */
  def findPictures(searchTerm: String) = Action.async {
    Future(Ok(Json.toJson(pictureCache.find(searchTerm))))
  }

}
