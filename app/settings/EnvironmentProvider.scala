package settings

/**
 * This object gets all Environment Variables provided
 */
object EnvironmentProvider {


  def getCacheUpdate: Int = sys.env.getOrElse("CACHE_TIME_UPDATE", "1000").toInt

  def tokenUpdateInterval: Int = sys.env.getOrElse("TOKEN_UPDATE_INTERVAL", "60").toInt

}
