package business

import play.api.libs.json.JsonNaming.SnakeCase
import play.api.libs.json.{Json, JsonConfiguration, OFormat}

case class ApiKey(apiKey: String)

case class Token(token: String)

case class SinglePicture(id: String, croppedPicture: String)


case class Pictures(pictures: List[SinglePicture])

case class PictureWithInfo(id: String, author: Option[String], camera: Option[String], tags: Option[String], croppedPicture: Option[String], fullPicture: Option[String]) {
  def toParsedImage: ParsedPicture = ParsedPicture(id, s"${id},${author.getOrElse("")},${camera.getOrElse("")},${tags.getOrElse("")},${croppedPicture.getOrElse("")},${fullPicture.getOrElse("")}")
}

object ApiKey {

  import play.api.libs.json._

  implicit val format: OFormat[ApiKey] = Json.format[ApiKey]
}

object Token {

  import play.api.libs.json._

  implicit val format: OFormat[Token] = Json.format[Token]
}

object SinglePicture {

  import play.api.libs.json._

  implicit val config = JsonConfiguration(SnakeCase)
  implicit val format: OFormat[SinglePicture] = Json.format[SinglePicture]
}

object Pictures {
  implicit val format: OFormat[Pictures] = Json.format[Pictures]
}

object PictureWithInfo {
  implicit val config = JsonConfiguration(SnakeCase)
  implicit val format: OFormat[PictureWithInfo] = Json.format[PictureWithInfo]
}
