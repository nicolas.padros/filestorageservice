package business

import settings.EnvironmentProvider

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * This class stores the cache from the server URL
  * @param pictureFetcher
  * @param exec
  */
class PictureCacheImpl(pictureFetcher: PictureFetcher)(
    implicit exec: ExecutionContext)
    extends PictureCache {

  var cache: List[ParsedPicture] = Await.result(getAll, 1.minute)
  println("Finished cache")

  //This thread is the one used for updating cache every X seconds
  val updateCacheThread = new Thread {
    override def run(): Unit = {
      var startTime = System.currentTimeMillis()
      val interval: Int = EnvironmentProvider.getCacheUpdate
      while (true) {
        val currentTime = System.currentTimeMillis()
        if (currentTime - startTime > (interval * 1000)) { //if the time threshold has exceeded the tolerance limit
          println("Updating Image cache")
          getAll.map(list => cache = list) // update the cache
          startTime = currentTime
        }
      }
    }
  }
  updateCacheThread.start()

  /**
    * Gets all pictures
    * @return
    */
  private def getAll: Future[List[ParsedPicture]] = getAllAux(0, List.empty)

  private def getAllAux(
      pageNumber: Int,
      acc: List[ParsedPicture]): Future[List[ParsedPicture]] = {
    getParsedImagesForNumber(pageNumber).flatMap {
      case Some(value) =>
        if (value.nonEmpty) getAllAux(pageNumber + 1, acc ::: value)
        else Future.successful(acc)
      case None => Future.successful(acc)
    }

  }

  private def getParsedImagesForNumber(
      pageNumber: Int): Future[Option[List[ParsedPicture]]] = {
    val eventualPicturesOption: Future[Option[Pictures]] =
      pictureFetcher.fetchImages(pageNumber)
    eventualPicturesOption.flatMap {
      case Some(pictures: Pictures) =>
        Future
          .sequence(pictures.pictures.map((picture: SinglePicture) =>
            pictureFetcher.getInfo(picture.id)))
          .map(_.filter(_.nonEmpty).map(_.get.toParsedImage))
          .map((list: List[ParsedPicture]) => Option(list))
      case None => Future.successful(None)
    }
  }

  /**
    * Finds all pictures that contain the searchQuery
    * @param searchQuery parameter to match
    *  @return a list of pictures that contain the key word
    */
  override def find(searchQuery: String): List[PictureWithInfo] = {
    cache.filter(_.allInfo.contains(searchQuery)).map(_.toPictureWithInfo)
  }

}

/**
  * Class stored in cache
  * @param id of picture
  * @param allInfo metadata stored in a single string
  */
case class ParsedPicture(id: String, allInfo: String) {
  def toPictureWithInfo: PictureWithInfo = {
    val infoValues = allInfo.split(",")
    PictureWithInfo(infoValues(0),
                    author = Some(infoValues(1)),
                    camera = Some(infoValues(2)),
                    tags = Some(infoValues(3)),
                    croppedPicture = Some(infoValues(4)),
                    fullPicture = Some(infoValues(5)))
  }
}

/**
  * This trait specifies the method to search all the cache
  */
trait PictureCache {

  /**
    * Finds all pictures that contain the searchQuery
    * @param searchQuery parameter to match
    * @return a list of pictures that contain the key word
    */
  def find(searchQuery: String): List[PictureWithInfo]
}
