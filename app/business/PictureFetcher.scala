package business

import play.api.libs.json.{JsError, JsPath, JsSuccess, Json, JsonValidationError}
import play.api.libs.ws.{WSClient, WSResponse}
import settings.EnvironmentProvider

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}

/**
 * This class handles all requests done to the AgileServerUrl
 * @param ws WSClient used for requests
 * @param exec Context for Futures
 */
class PictureFetcher(ws: WSClient)(implicit exec: ExecutionContext) {

  private val serverUrl = "http://interview.agileengine.com"
  private val apiKey = "23567b218376f79d9415"
  private var token: String = Await.result(retryToken, 10.seconds).token


  //Thread used for token update
  val updateTokenThread = new Thread {
    override def run(): Unit = {
        var startTime = System.currentTimeMillis()
        val interval: Int = EnvironmentProvider.getCacheUpdate
        while(true) {
          val currentTime = System.currentTimeMillis()
          if(currentTime - startTime > EnvironmentProvider.tokenUpdateInterval * 1000) { //renew the token
            getToken.map(tokenOpt => tokenOpt match {
              case Some(value) =>
                println("Token updated")
                token = value.token
              case None => println("Error in getting token")
            }) // update the cache
            startTime = currentTime
          }
        }
    }
  }

  updateTokenThread.start()


  private def retryToken: Future[Token] = {
    getToken.flatMap {
      case Some(value) => Future.successful(value)
      case None => retryToken
    }
  }

  /**
   * Gets token for server authentication
   * @return
   */
  private def getToken: Future[Option[Token]] =
    ws.url(serverUrl + "/auth").post(Json.toJson(ApiKey(apiKey))).map((response: WSResponse) => {
      if (response.status == 200) {
        val token: Token = response.json.as[Token]
        Some(token)
      } else {
        println("There was an error in getting the token")
        println(response.body)
        None
      }
    })


  /**
   * Fetches all images by pageNumber
   * @param pages number
   * @return a list of Pictures, or None if Error
   */
  def fetchImages(pages: Int = 0): Future[Option[Pictures]] = {
    val queryParam = pages match {
      case 0 => ""
      case other => s"?page=$other"
    }
    ws.url(serverUrl + "/images" + queryParam).addHttpHeaders(("Authorization" -> s"Bearer $token")).get().map(response => {
      response.json.validate[Pictures] match {
        case JsSuccess(value: Pictures, path) => Some(value)
        case JsError(errors) =>
          Json.prettyPrint(JsError.toJson(errors))
          println("There was an error fetching images")
          None
      }
    })
  }


  /**
   * Gets all the metadata for a specific picture
   * @param pictureId of picture
   * @return Some(info) if id found, None otherwise
   */
  def getInfo(pictureId: String): Future[Option[PictureWithInfo]] = {
    ws.url(serverUrl + s"/images/$pictureId").addHttpHeaders(("Authorization" -> s"Bearer $token")).get().map(response => {
      response.json.validate[PictureWithInfo] match {
        case JsSuccess(value, path) => Some(value)
        case JsError(errors) =>
          println(JsError.toJson(errors))
          println(s"There was an error getting info for imageId: $pictureId")
          None
      }
    })
  }

}
